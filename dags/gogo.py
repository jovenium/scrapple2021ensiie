# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""
### Tutorial Documentation
Documentation that goes along with the Airflow tutorial located
[here](https://airflow.incubator.apache.org/tutorial.html)
"""
from datetime import timedelta
from datetime import datetime
from scrapper import scrapple_API, scrapple_to_mongo
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from jsontomongo import JsonToMongoOperator
from apitojson import APIToJsonOperator

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'Jovenium',
    'depends_on_past': False,
    'start_date': datetime(2020, 2, 26),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'AppleStock',
    default_args=default_args,
    description='A simple test DAG',
    schedule_interval=timedelta(minutes=1),
    catchup = False
)

# t1, t2 and t3 are examples of tasks created by instantiating operators
t1 = APIToJsonOperator(
    task_id='APIProfile',
    name="profile",
    url="https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=",
    dag=dag,
)

t2 = APIToJsonOperator(
    task_id='APIRating',
    name="rating",
    url="https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=",
    dag=dag,
)


t1.doc_md = """\
#### Task Documentation
You can document your task using the attributes `doc_md` (markdown),
`doc` (plain text), `doc_rst`, `doc_json`, `doc_yaml` which gets
rendered in the UI's Task Instance Details page.
![img](http://montcs.bloomu.edu/~bobmon/Semesters/2012-01/491/import%20soul.png)
"""

dag.doc_md = __doc__

t3 = JsonToMongoOperator(
    task_id='MongoProfile',
    file_to_load="/tmp/profile.json",
    mongoserver= 'mongodb',
    mongouser= 'mongo',
    mongopass= 'mongo',
    dag=dag,
)

t4 = JsonToMongoOperator(
    task_id='MongoRating',
    file_to_load="/tmp/rating.json",
    mongoserver= 'mongodb',
    mongouser= 'mongo',
    mongopass= 'mongo',
    dag=dag,
)

t1 >> t3
t2 >> t4