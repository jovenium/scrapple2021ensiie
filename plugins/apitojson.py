# -*- coding: utf-8 -*-
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
from typing import List, Optional, Union

#from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.utils.decorators import apply_defaults
from airflow.plugins_manager import AirflowPlugin
from airflow.models import BaseOperator
import requests
from pymongo import MongoClient
from datetime import datetime
import json


class APIToJsonOperator(BaseOperator):

    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'

    @apply_defaults
    def __init__(
            self,
            name: str,
            url: str,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.name = name
        self.url = url
        self.api_key = "c78ce77596379cdd1dfbc5ad56d58f1b"

    def execute(self, context):
        #run code here to make the connection and
        print('started <--------------------')
        url_profile=self.url + self.api_key
        r = requests.get(url_profile)
        profile= r.json()
        data = {'profile':profile[0], 'timestamp': str(datetime.now())}
        json.dump( data, open( "/tmp/" + self.name +".json", 'w' ) )
        print('fin <--------------------')

class APIToJsonPlugin(AirflowPlugin):
    name = "APIToJsonPlugin"
    operators = [APIToJsonOperator]
    